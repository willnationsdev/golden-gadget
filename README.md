Golden Gadget 🌟
===

⛓️ GDScript utility library focused on functional programming (FP)

Status: ⚗️ **experimental** (all functions are covered by tests, but not everything is implemented and there may be API changes)

![](screenshot.png)

👉 More info at [main library file](goldenGadget/GoldenGadget.gd).
